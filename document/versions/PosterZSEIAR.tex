\documentclass[final]{beamer}

\usepackage[orientation=portrait,scale=1.24]{beamerposter} 
\setlength{\emergencystretch}{10em}

\usetheme{confposter} 
\usepackage{enumitem}
\usepackage{amsmath,amssymb,amsthm,bm,calc}
\usepackage{wrapfig}
\newcommand{\Cbb}{\mathbb{C}}
\newcommand{\Rbb}{\mathbb{R}}
\newcommand{\Zbb}{\mathbb{Z}}
\newcommand{\Nbb}{\mathbb{N}}
\def\parc#1#2{\tfrac{\partial #1}{\partial #2}}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\setbeamercolor{block title}{fg=dblue,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size

\newlength{\sepwid}
\newlength{\sepwids}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\sepwid}{0.015\textwidth} % Separation width (white space) between columns
\setlength{\sepwids}{0.02\textwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.3\textwidth} % Width of one column
\setlength{\twocolwid}{0.602\textwidth} % Width of two columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size

%skip after/below equations
\setlength{\abovedisplayskip}{10pt}
\setlength{\belowdisplayskip}{10pt}
%-----------------------------------------------------------

\usepackage{graphicx}  % Required for including images

\usepackage{booktabs} % Top and bottom rules for tables

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{Model ZSEIAR} % Poster title

\author{ Lenka P\v{r}ibylov\'a (email:  \href{mailto:pribylova@math.muni.cz}{pribylova@math.muni.cz}), Veronika Hajnov\'a 
(email:  \href{mailto:hajnova@math.muni.cz}{hajnova@math.muni.cz})} % Author(s)

\institute{Department of Mathematics and Statistics, Faculty of Science, Masaryk University, Brno, Czech Republic} % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{columns}[t]
\begin{column}{\twocolwid} % 1. dvojice sloupcu
\begin{block}{METHODICS}
\begin{columns}[t,totalwidth=\twocolwid] 
\setbeamercolor{block title}{fg=dblue,bg=white}

\begin{column}{\onecolwid} 

\begin{alertblock}{Differential equations}
\begin{align*}
\dot Z = & - \varepsilon Z/N,\\
\dot S = & - \tfrac{\beta}{N-Z} S (I+A) + \varepsilon Z/N,\\
\dot E = &  \tfrac{\beta}{N-Z} S (I+A) - \gamma E,\\
\dot I = &  \gamma p E - \mu_1 I,\\
\dot A = &  \gamma (1-p) E - \mu_2 A,\\
\dot Q = &  \mu_1 I - \nu Q,\\
\dot R = & \nu Q, 
\end{align*}
\end{alertblock} 
\begin{wrapfigure}{r}{0.4\textwidth}
\begin{center}
    \includegraphics[width=0.17\textwidth]{ZSEIAR.eps}
  \end{center}
\end{wrapfigure}

%\begin{itemize}[noitemsep,topsep=3pt]
\phantom{--} \makebox[1em][l]{$Z$} not affected population size \\
\phantom{--} \makebox[1em][l]{$S$} susceptibles\\
\phantom{--} \makebox[1em][l]{$E$} exposed\\
\phantom{--} \makebox[1em][l]{$I$} detected infectious\\
\phantom{--} \makebox[1em][l]{$A$} undetected infectious\\
\phantom{--} \makebox[1em][l]{$Q$} izolated infectious\\
\phantom{--} \makebox[1em][l]{$R$} removed detected\\
\phantom{--} \makebox[1em][l]{$N$} population size\\
\phantom{--} \makebox[1em][l]{$p$} ascertainment rate\\
\phantom{--} \makebox[7em][l]{$\varepsilon, \beta, \gamma, \mu_1, \mu_2, \nu$} parameters\\
%\end{itemize}
 
\bigskip

Optimization estimates the size of the affected clusters (dependencies such as seasonality, the degree of influence of government measures or changes in people's behavior caused by fear or disinformation are included in the estimate of affected clusters), we use estimates of latent or infectious periods from the literature, estimates of computable periods as isolation time or time to hospitalization or death from \'UZIS dataset and a 14-day moving average ascertainment rate estimate. Scenarios of ZSEIAR model can be generated using MAMES application \cite{MAMES}, for detailed documentation see \cite{doc}.
\bigskip

\begin{block}{Mobility dependence}
Transmisibility rate $\beta$ is assumed to be strictly dependent on the number of contacts or mobility. 
\begin{figure}[H]
\includegraphics[width=\textwidth]{Dan.pdf}
\caption{ \quad Number of contacts according to social studies \cite{DanProkop} during COVID-19 epidemic in the Czech Republic, 2020}
\end{figure}
\begin{figure}[H]
\includegraphics[width=\textwidth]{Mob.pdf}
\caption{ \quad Mobility \cite{GoogleMobility} during COVID-19 epidemic in the Czech Republic, 2020}
\end{figure}
\end{block}
\end{column} 

\begin{column}{\sepwids}\end{column}

\begin{column}{\onecolwid}

\begin{alertblock}{Ascertainment rate estimate}
The rate is based on the Bayes rule for conditional probabilities and on the assumption that the average infected person's probability of hospitalization is given or estimated.

\begin{equation*}
\label{Bayes}
p=P(Det)=\frac{P(Det|H) P(H)}{P(H|Det)}
\end{equation*}
\end{alertblock}
{\fontsize{1pt}{1pt}\selectfont}
$P(Det|H)$ -- the probability that a person hospitalized with COVID-19 was previously detected;  the proportion of patients detected  prior to admission to hospital and all patients hospitalized with COVID-19 (including those not detected prior to admission to hospital) relative to the date of the positive test report.\\
$P(H|Det)$ -- the probability that if an individual was detected, he or she will be hospitalized; the proportion of all reported hospitalized patients detected before admission to the hospital, and all detected persons except those detected only in the hospital.\\
$P(H)$ -- the probability that SARS-CoV-2 positive individual is/was/will be hospitalized (regardless of whether was detected or not); estimated to the age structure of the epidemics affected.  
\bigskip

\begin{block}{Hospitalization probability age structure dependence}
Age structure in the Czech Republic attained $20\%$ share of the over 65 year-old population. Patients hospitalized with COVID-19 over the age of 65 had a long-term ratio of around $3/4$ during the autumn 2020, so a rough estimate of probability $P(H_{65+})$ of hospitalization with COVID-19 for a person over 65 is around twelve times higher than probability $P(H_{65-})$ of hospitalization with COVID-19 for a person under 65.
\begin{align*}
P(H)&=p^+_{65-}P(H_{65-})+p^+_{65+}P(H_{65+})=\\
&=\biggl (1 + 11 \tfrac{p^+_{65+}}{p^+_{65-}} \biggr ) P(H_{65-}),
\end{align*}
where $p^+_{65+}$ and $p^+_{65-}$ are 7-day moving averages of the senior and non-senior population ratio in the reported positive cases.
\end{block}

\begin{alertblock}{Early warning}
A significant decrease in the ascertainment rate estimate is a signal of tracing and testing overload collapse.
\end{alertblock}
\begin{figure}[H]
\includegraphics[width=\textwidth]{failKHS.pdf}
\caption{\quad The ascertainment rate estimate shows the collapse of tracing by regional hygiene stations, which occurred in the second half of September. }
\end{figure}
\begin{block}{Acknowledgements}
\small{\rmfamily{This work was supported by grants Online platform for real-time monitoring, analysis and management of epidemic situations number 
MUNI/11/02202001/2020 and Mathematical and statistical modelling number MUNI/A/1615/2020.}}
\end{block}
\end{column}


\end{columns}
\end{block}
\end{column}
% konec 1.dvojsloupce

%\begin{column}{\sepwids} \end{column}

\begin{column}{\onecolwid} 
\begin{block}{REAL DATA FIT} 
\setbeamercolor{block title}{fg=dblue,bg=white}
\begin{block}{Spring and summer 2020}
 Simulation of a spring outbreak in the Czech Republic with the constant growth rate of affected clusters copies the real data due to the ascertainment rate estimate that shapes the local outbreak in the OKD mines that was the main source of the infected in the Czech Republic in July. In August, the optimization of affected clusters abruptly changes and is no longer constant as the real data start to increase exponentially.
\begin{figure}[H]
\includegraphics[width=\textwidth]{activecases.pdf}
\caption{\quad  Active cases -- real data (circles), ZSEIAR model with constant $\varepsilon$ from April (blue), and optimized $\varepsilon$ (red)}
\end{figure}
\end{block}

\begin{block}{Fall 2020}
\begin{figure}[H]
\vspace{-0.7cm}
\includegraphics[width=\textwidth]{hosp.pdf}
\vspace{-0.7cm}
\includegraphics[width=\textwidth]{dead.pdf}
\caption{\quad Optimalized fit (line) to real data (circles) during the year 2020 -- incidence of admissions to hospitals and deaths.}
\end{figure}
\end{block}

\vspace{-0.6cm}
\begin{block}{References}
\begin{thebibliography}{9}
\bibitem{DanProkop} PAQ research, IDEA AntiCovid, \url{https://zivotbehempandemie.cz/kontakty}, 2020 (accessed January 21, 2021)
\bibitem{GoogleMobility} Google Mobility reports, \url{https://github.com/ActiveConclusion/COVID19_mobility/blob/master/google_reports/mobility_report_europe.xlsx}, 2020 (accessed January 21, 2021)
\bibitem{MAMES} MAMES application, \url{https://webstudio.shinyapps.io/MAMES/} 
\bibitem{doc} ZSEIAR model (documentation in Czech), \url{https://is.muni.cz/www/98951/47857356/mames/MAMES_ZSEIAR_metodika.pdf}, 2021 
\end{thebibliography}
\end{block}
\end{block}
\end{column}
\end{columns}
\end{document}
