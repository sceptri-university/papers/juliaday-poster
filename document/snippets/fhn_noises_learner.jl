using Revise
using DataDrivenDiffEq, DataDrivenSparse
using ModelingToolkit, DifferentialEquations
using LinearAlgebra, Random
using CairoMakie, ColorSchemes, LaTeXStrings
using NoiseRobustDifferentiation
using Serialization

# My own packages
include("../src/custom.jl")
using .Custom
using .Custom.Noiser
using DynamicSTLS

begin
    optimizers = []
    μ = 0.025
    regularization = 0
    λ = 0
    k = 1
    noise_range = []
end

Random.seed!(12345)

trajectory_count = 10
data_variables = (:V, :W)

noise_range = 0.001:0.005:0.1
optimizers = [STLSQ(0.08), SR3(0.1, 1.0), DSTLS(0.15)]

λ = [0.3, 0.3];
k = [4, 5];
number_of_iterations = [30, 30]
regularization = [0.08, 0.9]

noise_type = AdditiveVarianceNoise

parameters_all = Array{Float32}(undef, (length(optimizers), length(noise_range), trajectory_count))
rss_all = Array{Float32}(undef, (length(optimizers), length(noise_range), trajectory_count))
r2_all = Array{Float32}(undef, (length(optimizers), length(noise_range), trajectory_count))
aic_all = Array{Float32}(undef, (length(optimizers), length(noise_range), trajectory_count))
bic_all = Array{Float32}(undef, (length(optimizers), length(noise_range), trajectory_count))

term_library = variables -> polynomial_basis(variables, 4)

experiment_hash = string(hash(noise_range)) * "-" * string(hash(string.(optimizers)))

if isfile(here("../data/$experiment_hash-full_data.dat"))
    full_data = deserialize(here("../data/$experiment_hash-full_data.dat"))

	parameters_all = full_data.params
	rss_all = full_data.rss
	r2_all = full_data.r2
	aic_all = full_data.aic
	bic_all = full_data.bic
else
	for trajectory_index in 1:trajectory_count
		times = results.times[trajectory_index]
		true_trajectory = results.trajectories[trajectory_index]'

		for (index, absolute_noise) in enumerate(noise_range)
			@show trajectory_index, index
			noise = noise_type(0, absolute_noise)
			noisy_trajectory = noise(true_trajectory)
			denoised_trajectory = denoise(noisy_trajectory, k, λ)

			begin
				resolution = length(times)
				time_length = times[end] - times[1]
				each_step = time_length / resolution
			end

			tvdiff_parameters = (
				dx=each_step,
				# diff_kernel="square",
				scale="large",
				ε=1e-10,
			)

			tvdiff_df = tvdiff(noisy_trajectory, number_of_iterations, regularization; tvdiff_parameters...)

			@named data_driven_problem = ContinuousDataDrivenProblem(
				denoised_trajectory,
				times,
				tvdiff_df)

			@variables V, W
			variables = [V, W]

			@named model_basis = Basis(term_library(variables), variables)

			for (opt_index, optimizer) in enumerate(optimizers)
				try
					fitted_solution = solve(data_driven_problem, model_basis, optimizer)
					fitted_system = get_basis(fitted_solution)
					fitted_parameters = get_parameter_map(fitted_system)

					parameters_all[opt_index, index, trajectory_index] = length(fitted_parameters)
					rss_all[opt_index, index, trajectory_index] = rss(fitted_solution)
					r2_all[opt_index, index, trajectory_index] = r2(fitted_solution)
					aic_all[opt_index, index, trajectory_index] = aic(fitted_solution)
					bic_all[opt_index, index, trajectory_index] = bic(fitted_solution)
				catch
					parameters_all[opt_index, index, trajectory_index] = NaN
					rss_all[opt_index, index, trajectory_index] = NaN
					r2_all[opt_index, index, trajectory_index] = NaN
					aic_all[opt_index, index, trajectory_index] = NaN
					bic_all[opt_index, index, trajectory_index] = NaN
				end
			end
		end
	end
end

serialize(here("../data/$experiment_hash-full_data.dat"), (
	params=parameters_all,
	rss=rss_all,
	r2=r2_all,
	aic=aic_all,
	bic=bic_all
))

parameters_ribbon = Custom.calculate_ribbon(parameters_all)
rss_ribbon = Custom.calculate_ribbon(rss_all)
r2_ribbon = Custom.calculate_ribbon(r2_all)
aic_ribbon = Custom.calculate_ribbon(aic_all)
bic_ribbon = Custom.calculate_ribbon(bic_all)

labels = [
    "STLS",
    "SR3",
    "DSTLS"
]

figure = Custom.noise_comparison_plot(noise_range, labels, parameters_ribbon, r2_ribbon, aic_ribbon, bic_ribbon;
	xlabel=L"\hat{\sigma} \text{ of AWGN scaled with variance of data}",
	title=nothing,
	transform=false, bottom_legend=true,
    resolution=(385, 500),
    colormap=:Zissou1Continuous
)