using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using Random

# My own packages
include("../src/custom.jl")
using .Custom

Random.seed!(123)

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) m(t) h(t) n(t)
@parameters Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

include(here("../snippets/hh.jl"))

# Parameter values taken from https://doi.org/10.1017/CBO9780511815706 (for us Gerstner2002-it)
params = [
    Cₘ => 1, # 1 Or Cₘ => 5
    Iₑ => 9, # 0 or 80 or 170 or 9.78 or 154.527
    gₙ => 120,
    Vₙ => 115,
    gₖ => 36,
    Vₖ => -12,
    gₗ => 0.3,
    Vₗ => 10.6
]
starting_values = [-20, 0.5, 0.5, 0.5]
time_range = (0.0, 100.0)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, m, h, n, Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ),
    D(m) ~ ṁ(V, m, h, n),
    D(h) ~ ḣ(V, m, h, n),
    D(n) ~ ṅ(V, m, h, n)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

# We expect the ODE problem to be stiff, so by default we use
# the Rosenbrock23 method
saveat = 0.25
solution = solve(ode_problem, Rosenbrock23(); saveat)

# For ease of handling, we create a temporary object to hide away saving logic
results = Custom.process_solution(
    solution,
    (:V, :m, :h, :n), # names of variables
    (coords, t) -> [
        V̇(coords..., last.(params)...),
        ṁ(coords...),
        ḣ(coords...),
        ṅ(coords...)
    ]
)