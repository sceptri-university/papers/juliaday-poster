# Helper functions for our system
# For m
αₘ(V) = 0.1 * (25 - V) / (exp((25 - V) / 10) - 1)
βₘ(V) = 4 * exp(-V / 18)
# For h
αₕ(V) = 0.07 * exp(-V / 20)
βₕ(V) = 1 / (exp((30 - V) / 10) + 1)
# For n
αₙ(V) = 0.01 * (10 - V) / (exp((10 - V) / 10) - 1)
βₙ(V) = 0.125 * exp(-V / 80)

# Governing equation(s) for our system
V̇(V, m, h, n, Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ) = 1 / Cₘ * (Iₑ - gₙ * m^3 * h * (V - Vₙ) - gₖ * n^4 * (V - Vₖ) - gₗ * (V - Vₗ))
ṁ(V, m, h, n) = αₘ(V) * (1 - m) - βₘ(V) * m
ḣ(V, m, h, n) = αₕ(V) * (1 - h) - βₕ(V) * h
ṅ(V, m, h, n) = αₙ(V) * (1 - n) - βₙ(V) * n