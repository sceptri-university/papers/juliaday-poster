using Revise
using ModelingToolkit, DifferentialEquations
using LinearAlgebra, Random, Statistics
using CairoMakie, ColorSchemes, LaTeXStrings

using ForwardDiff, ForwardDiffChainRules
using Zygote, ImplicitDifferentiation
using Optim

# My own packages
include("../src/custom.jl")
using .Custom, .Custom.Noiser

include("../snippets/hh.jl")

hh_variables = (:V, :m, :h, :n)

get_initial_cond(Λ, u_obs) = (Λ*u_obs)[:, 1]

absinf(x) = max(abs.(x))

to_state(p, Λ) = [p..., Λ...]
from_state(p_Λ) = (p_Λ[1:5], reshape(p_Λ[6:end], (2, 4)))

L₂_loss(estimate, truth; func=abs2) = sum(func, estimate .- truth)
R²_loss(estimate, truth; func=abs2) = L₂_loss(estimate, truth; func) / L₂_loss(truth, mean(truth); func)

function ode_loss(estimate, truth; func=abs2)
    return mean(func, estimate .- truth) + # calculate "residuals" of fitted trajectory
           α * abs(1 - var(estimate) / var(truth)) + # check that it has the same standard deviation
           β * abs(1 - var(estimate[:, 1:(end÷2)]) / var(estimate[:, (end÷2+1):end]))
    # check that estimate st. deviation is the same across the data
end

u_used = results.trajectory'
du_used = results.derivatives'
t_used = results.times

α = 50
β = 1

Λ₀ = [
    1.0 -36.0 0.0 0.0
    0.0 0.5 0.5 -0.5
]
p₀ = [0.45, 1.0, 0.8, 0.7, 0.8]

optim_iterations = 10000

# Define the differential equation
function FHN(u, p, t)
    V, W = u
    a, b, c, d, iₑ = p
    dV = V - V^3 / 3 - W + iₑ
    dW = a * (b * V - c * W + d)
    return [dV, dW]
end

function linear_transform_cost(p_Λ, u_obs, du_obs, t_obs)
    p, Λ = from_state(p_Λ)
    IC = get_initial_cond(Λ, u_obs)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    trajectory = hcat(reshape.(sol.u, 2, 1)...)
    transformed = (Λ*u_obs)[:, 1:size(trajectory, 2)]

    return ode_loss(trajectory, transformed)
end

# Find the optimal parameters
result = Optim.optimize(p_Λ -> linear_transform_cost(p_Λ, u_used, du_used, t_used), to_state(p₀, Λ₀), Optim.Options(iterations=optim_iterations))
p_optimal, Λ_optimal = from_state(result.minimizer)
IC_optimal = get_initial_cond(Λ_optimal, u_used)

prob = ODEProblem(FHN, IC_optimal, (t_used[1], t_used[end]), p_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used[:, 1:size(trajectory, 2)]
du_part = du_used[:, 1:size(trajectory_dt, 2)]
t_part = t_used[1:size(trajectory, 2)]

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_part)

figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
    Custom.plot_linear_transformation(Λ_optimal * u_part, trajectory, -6 .. 6, -2.5 .. 4, f;
        legend_outside=false,
        shift_colors=true,
        show_legend=true,
        show_title=false,
        resolution=(430, 345),
        position=:lb,
        native_label=L"\text{native FHN}",
        transform_label=L"\text{lin. transf. HH}",
        colormap=:Zissou1Continuous
    )
end