using Printf, LaTeXStrings

parameters_values(params) = [last(param) for param in params]

to_matrix(trajectory) = vcat(reshape.(trajectory, 1, length(trajectory[end]))...)

calc_derivatives(derivatives, trajectory, parameters) = [variable_derivative(state..., parameters...) for state in eachrow(trajectory), variable_derivative in derivatives]

import Base.show
import DataDrivenDiffEq: DataDrivenSolution, Basis

function show(io::IO, whole_solution::Tuple{DataDrivenSolution,Basis,Vector})
    solution, system, parameters = whole_solution

    println(io, system)
    println(io, "")
    println(io, solution)
    println(io, "")
    println(io, "Parameters:")
    println(io, parameters)
end

sprintf(format, values...) = Printf.format(Printf.Format(format), values...)

function to_string(strings::AbstractVector{String}; format_to="%s", join_by=',', do_join=true)
    formatted = [("\$" * sprintf(format_to, one_string) * "\$") for one_string in strings]
    return do_join ? (join(formatted, join_by)) : formatted
end

to_string(symbols::AbstractVector{Symbol}; kwargs...) = to_string([String(symbol) for symbol in symbols]; kwargs...)
to_string(symbol::Symbol; kwargs...) = to_string([symbol]; kwargs...)
to_string(one_string::String; kwargs...) = to_string([one_string]; kwargs...)
to_string(tuple::Tuple; kwargs...) = to_string(collect(tuple); kwargs...)
to_string(string::AbstractString; more...) = string
to_string(string::LaTeXString; more...) = string

import ModelingToolkit: ODESystem

function system_to_functions(system::ODESystem, system_variables, parameter_map)
    @assert length(system_variables) == length(system.eqs) "Same number of input variables and equations in system must be supplied"

    rhs_functions = []

    for equation in system.eqs
        args_function = substitute(equation.rhs, parameter_map)
        func_expr = build_function(args_function, system_variables...)

        push!(rhs_functions, eval(func_expr))
    end

    return (vec...) -> [rhs_function(vec...) for rhs_function in rhs_functions]
end

import DataDrivenDiffEq: DataDrivenProblem

function calculate_value_space(func, x_bounds, y_bounds;
    steps=300
)
    r₁ = range(minimum(x_bounds), stop=maximum(x_bounds), length=steps)
    r₂ = range(minimum(y_bounds), stop=maximum(y_bounds), length=steps)

    f₁ = (x, y) -> func(x, y)[1]
    f₂ = (x, y) -> func(x, y)[2]

    return f₁.(r₁, r₂'), f₂.(r₁, r₂')
end

differential_to_string(diff) = string(diff)[end-1] |> string
remove_unicode(message) = replace(message, ['₀' + i => i for i in 0:9]...)
make_list(array; by=',') = join(array, by)

function dominant_frequency(signal, sampling_rate)
    signal_step_diference = signal[2:end] .- signal[1:end-1]
    # Find where the signal is incresing, resp. decreasing
    signal_ups = signal_step_diference .> 0
    signal_downs = signal_step_diference .< 0

    # Find where the monotony changes from up to down
    signal_monotony = signal_ups[2:end] .&& signal_downs[1:end-1]

    # Compute mean distance between ups
    ups = findall(identity, signal_monotony)
    avg_distance = mean(ups[2:end] .- ups[1:end-1])

    return avg_distance ≈ 0 ? NaN : sampling_rate / avg_distance
end

function to_ODE_system(fitted_system; name=:fitted_system)
    return ODESystem(
        equations(fitted_system),
        get_iv(fitted_system),
        states(fitted_system),
        parameters(fitted_system);
        checks=false,
        name
    )
end

using ModelingToolkit, DifferentialEquations, Statistics

function to_results(problem, basis, timespan, saveat)
    # For each sample define starting values, timespan and parameters
    starting_values = problem.X[:, 1]
    fitted_parameters = get_parameter_map(basis)

	predictive_system = to_ODE_system(basis)

    # Computing the prediction for each sample
    ode_predictive_problem = ODEProblem(predictive_system, starting_values, timespan, fitted_parameters)
    prediction = solve(ode_predictive_problem, Rosenbrock23(); saveat)
    prediction_trajectory = to_matrix(prediction.u)

	@variables V,W

    # Make fitted system callable
    fitted_functions = system_to_functions(predictive_system, [V, W], fitted_parameters)
    point2_functions = (V, W) -> Point2f(fitted_functions(V, W))

    # Compute dominant frequency for each sample
    sampling_rate = 1 / (problem.t[2] - problem.t[1])
    frequencies = [dominant_frequency(var_trajectory, sampling_rate) for var_trajectory in eachcol(prediction_trajectory)]

    # Return tuple of useful precomputed info
    return (
        prediction=prediction_trajectory,
		functions=point2_functions,
		frequencies=frequencies,
        times=timespan[begin]:saveat:timespan[end]
    )
end

function as_tex(basis::Basis;
    delimiter=",\\\\",
    remove_parantheses=true,
    add_cdot=true,
    digits=2,
    lhs="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}",
    format_var="\\hat{%s}",
    aligned=true,
    line_length=150,
    aligned_break="\\\\ \n\t&",
    aligned_equals="=&\\hphantom{+}",
	environment = "gather"
)
    params = get_parameter_map(basis)
    params = map((pair -> (first(pair) => round(last(pair); digits))), params)
    vars = states(basis)

    operators = ['+', '-']

    tex_system = isempty(environment) ? "" : "\\begin{$environment}\n"

    for (eq_index, eq) in enumerate(equations(basis))
        eq_subtituted = substitute(eq.rhs, params)
        eq_string = string(eq_subtituted)


        if remove_parantheses
            eq_string = replace(eq_string, '(' => "", ')' => "")
        end

        if add_cdot
            eq_string = replace(eq_string, '*' => "")
            format = " \\cdot $format_var "
        end
        for var in string.(vars)
            eq_string = replace(eq_string, var => sprintf(format, string(var)))
        end

        split_lines = false
        lines = ceil(Int64, length(eq_string) / line_length)
        while lines > 1
            split_lines = true
            char_index = (lines - 1) * line_length
            while !(eq_string[char_index] in operators) && char_index > 0
                char_index -= 1
            end

            if char_index == 0
                break
            end

            eq_string = eq_string[1:(char_index-1)] * aligned_break * eq_string[char_index:end]
            lines -= 1
            char_index = (lines - 1) * line_length
        end

        var = string(vars[eq_index])
        lhs_var = sprintf(lhs, string(var))

        equates = (split_lines || aligned) ? aligned_equals : "="
        tex_system *= "$lhs_var $equates $eq_string"

        if (eq_index != length(vars))
            tex_system *= delimiter * "\n"
        end
    end

    tex_system *= isempty(environment) ? "" : "\n\\end{$environment}"

    return tex_system
end

import FileIO.save
function save(filename, data::String)
    open(filename, "w+") do io
        println(io, data)
    end
end

here(filename::AbstractString) = joinpath(Base.source_dir(), filename)

"""
	function process_solution(
		solution::EnsembleSolution, 
		variables::Tuple, 
		derivative_function::Base.Callable=identity
	)

Processes the solution to a tuple (straight from the EnsembleSolution)
"""
function process_solution(solution::EnsembleSolution, variables::Tuple, derivative_function::Base.Callable=identity)
	processed_trajectories = []
	processed_derivatives = []
	processed_times = []

    # Transforming trajectories from an EnsembleProblem to an array of matrices representing the trajectories
    for singular_solution in solution
        trajectory = mapreduce(permutedims, vcat, singular_solution.u)
        push!(processed_trajectories, trajectory)
        push!(processed_times, singular_solution.t)
        derivatives = [derivative_function(coords, singular_solution.t[t_index]) for (t_index, coords) in enumerate(eachrow(trajectory))]
        push!(processed_derivatives, mapreduce(permutedims, vcat, derivatives))
    end

    return (
		variables = variables,
		trajectories = processed_trajectories,
		derivatives = processed_derivatives,
		times = processed_times
	)
end

"""
	function process_solution(
		solution::EnsembleSolution, 
		variables::Tuple, 
		derivative_function::Base.Callable=identity
	)
"""
function process_solution(solution::ODESolution, variables::Tuple, derivative_function::Base.Callable=identity)
    trajectory = to_matrix(solution.u)

    derivatives = derivative_function.([row for row in eachrow(trajectory)], solution.t)
    derivatives_trajectory = mapreduce(permutedims, vcat, derivatives)

    return (
        variables=variables,
        trajectory=trajectory,
        derivatives=derivatives_trajectory,
        times=solution.t
    )
end

function process_solution(solution::AbstractMatrix, solution_times::AbstractArray, variables::Tuple)
    processed_trajectories = []
    processed_times = []

    push!(processed_trajectories, solution)
    push!(processed_times, solution_times)

    return (
        variables=variables,
        trajectories=processed_trajectories,
        times=processed_times
    )
end

function calculate_ribbon(data::Array{T}) where {T<:Number}
    @assert length(size(data)) == 3
    ribbon = Array{T}(undef, (size(data)[1], size(data)[2], 3))
    for (row_index, row) in enumerate(eachslice(data, dims=1))
        for (σ_index, values_for_σ) in enumerate(eachslice(row, dims=1))
            ribbon[row_index, σ_index, 1] = mean(values_for_σ)
            ribbon[row_index, σ_index, 2] = minimum(values_for_σ)
            ribbon[row_index, σ_index, 3] = maximum(values_for_σ)
        end
    end

    return ribbon
end