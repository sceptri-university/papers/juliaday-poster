using Makie, MakieCore, LaTeXStrings, ColorSchemes
using DataDrivenDiffEq

export fetch_colors
export dataproblem_plot, derivative_comparison_plot
export noisy_trajectory_comparison_plot, fitted_model_plot, predicted_trajectory_comparison_plot
export generated_trajectory_plot, phase_space_plot
export each_derivative_comparison_plot, each_predicted_trajectory_comparison_plot
export noise_comparison_plot, attractor_plot, plot_linear_transformation
export linear_transformation_comparison
export plot_noise_on_points, plot_noise_on_func
export false_phase_portrait

"""
Fetches a given amount of colors from a colormap

It is to be used with colormaps to ensure a "unified" look

## Options
- `palette` - determines whether to return a tuple ready to be used for Axis palette option
"""
function fetch_colors(colormap::Symbol, count::Integer; rev=false, palette=true, transform=false)
    color_points = count > 1 ? range(0, 1; length=count) : [0]
    if transform
        func(x) = mean([x, 0.5])
        color_points = func.(color_points)
    end
    color_points = rev ? 1 .- color_points : color_points
    colour = get(colorschemes[colormap], color_points)
    return palette ? (color=colour, patchcolor=colour) : colour
end

# ------| PLOTTING FUNCTIONS |--------
# We're NOT using @recipe macro, because it doesn't play well with Axis and stuff like that
# This is one of the areas Makie isn't really mature enough for everyday use in

"""
	dataproblemsummary!(axis::Axis, data_driven_problem::DataDrivenProblem; colormap=:Nizami, trajectory=true, labels=["V", "W"])
Helper function to make plotting the summary of a DataDrivenProblem easier

**Arguments**:
- `axis::Axis`
- `data_driven_problem::DataDrivenProblem`

**Keyword Arguments**:
- `colormap` - colormap to be used when plotting
- `trajectory` - toggle whether to plot trajectory of derivatives
- `labels` - labels for each state variable
"""
function dataproblemsummary!(axis::Axis, data_driven_problem::DataDrivenProblem;
    colormap=:Nizami,
    trajectory=true,
    labels=["V", "W"]
)
    time = data_driven_problem.t
    data = trajectory ? data_driven_problem.X : data_driven_problem.DX

    palette = fetch_colors(colormap, size(data, 1); palette=false)

    for (index, row) in enumerate(eachrow(data))
        lines!(axis, time, row; color=palette[index], linewidht=1, label=(index <= length(labels) ? LaTeXString(labels[index]) : ""))
    end

    return axis
end

"""
	function dataproblem_plot(problem::T;
		resolution=(700, 600),
		colormap=:Nizami,
		variables=(:V, :W),
		time_label=:t,
		title=L"\\text{\\textbf{Data problem summary}}",
		position=:rt
	) where {T<:DataDrivenProblem}

User facing function to plot complete summary of DataDrivenProblem, using helper function `dataproblemsummary`

**Arguments:**
- `problem::DataDrivenProblem`

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `variables` - variable names used legends
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
"""
function dataproblem_plot(problem::T;
    resolution=(700, 600),
    colormap=:Nizami,
    variables=(:V, :W),
    time_label=:t,
    title=L"\text{\textbf{Data problem summary}}",
    position=:rt
) where {T<:DataDrivenProblem}
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables)),
        title
    )
    dataproblemsummary!(axis, problem; colormap, labels=to_string(variables, format_to="%s(t)", do_join=false))
    axislegend(axis; position)

    axis = Axis(figure[2, 1],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\dot{%s}"))
    )
    dataproblemsummary!(axis, problem;
        trajectory=false,
        colormap,
        labels=to_string(variables; format_to="\\dot{%s}(t)", do_join=false)
    )

    axislegend(; position)
    figure
end

"""
	function derivative_comparison_plot(time, derivative, truth, variables;
		resolution=(700, 500),
		time_label=:t,
		title=L"\\text{\\textbf{Derivatives}}",
		derivative_name="tvdiff",
		position=:rb,
		colormap=:Nizami
	)

Plots the comparison of derivatives in each variable - plots them all into a single axis

**Arguments**:
- `time` - vector of time measurements
- `derivative` - found derivative to compare against the ground truth
- `truth` - true derivative
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
- `derivative_name` - name of the supplied derivative displayed in the legend
"""
function derivative_comparison_plot(time, derivative, truth, variables;
    resolution=(700, 500),
    time_label=:t,
    title=L"\text{\textbf{Derivatives}}",
    derivative_name="tvdiff",
    position=:rb,
    colormap=:Nizami
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        title,
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\dot{%s}")),
        palette=fetch_colors(colormap, length(variables))
    )

    for (variable_index, variable_name) in enumerate(variables)
        lines!(axis, time, derivative'[:, variable_index], label=LaTeXString(derivative_name * " \$\\dot{$variable_name}\$"), color=Cycled(variable_index))
        lines!(axis, time, truth'[:, variable_index], label=L"\text{true } \dot{%$variable_name}", linestyle=:dashdot, color=Cycled(variable_index))
    end

    axislegend(; position)
    figure
end


"""
	function each_derivative_comparison_plot(time, derivative, truth, variables;
		resolution=(590, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Derivatives}}",
		position=:rb,
		colormap=:Nizami,
		legend_outside=false,
		found_alpha=1
	)

Plots the comparison of derivatives in each variable - plots them all into separate axes

**Arguments**:
- `time` - vector of time measurements
- `derivative` - found derivative to compare against the ground truth
- `truth` - true derivative
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
- `legend_outside` - signals whether legend should be inside or outside of the main axes
- `found_alpha` - alpha channel of the color for the found derivative
"""
function each_derivative_comparison_plot(time, derivative, truth, variables;
    resolution=(590, 450),
    time_label=:t,
    title=L"\text{\textbf{Derivatives}}",
    position=:rb,
    colormap=:Nizami,
    legend_outside=false,
    found_alpha=1
)
    figure = Figure(; resolution)
    palette = fetch_colors(colormap, length(variables))
    colors = fetch_colors(colormap, length(variables); palette=false)

    for (variable_index, variable_name) in enumerate(variables)
        axis = Axis(figure[variable_index, 1];
            title=variable_index == 1 ? title : "",
            xlabel=variable_index == length(variables) ? LaTeXString(to_string(time_label)) : "",
            ylabel=LaTeXString(to_string(variable_name; format_to="\\dot{%s}")),
            palette
        )

        lines!(axis, time, derivative'[:, variable_index], label=L"\text{tvdiff } \dot{%$variable_name}", color=(colors[variable_index], found_alpha))
        lines!(axis, time, truth'[:, variable_index], label=L"\text{true } \dot{%$variable_name}", linestyle=:dashdot, color=Cycled(variable_index))
        if legend_outside
            Legend(figure[variable_index, 2], axis)
        else
            axislegend(; position)
        end
    end

    figure
end

"""
	function noisy_trajectory_comparison_plot(time, denoised, noisy, truth, variables;
		resolution=(600, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Trajectory}}",
		position=:rb,
		legend_outside=false,
		colormap=:Nizami,
		linewidth=1,
		noisy_alpha=1
	)

Plots the comparison of trajectories (true and noisy) in each variable - plots them all into separate axes

**Arguments**:
- `time` - vector of time measurements
- `denoised` - denoised trajectory (aka denoised `noisy`)
- `noisy` - true trajectory, but with added noise
- `truth` - true trajectory
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
- `legend_outside` - signals whether legend should be inside or outside of the main axes
- `noisy_alpha` - alpha channel of the color for the noisy trajectory
- `linewidth` - linewidth used when plotting trajectories 
"""
function noisy_trajectory_comparison_plot(time, denoised, noisy, truth, variables;
    resolution=(600, 450),
    time_label=:t,
    title=L"\text{\textbf{Trajectory}}",
    position=:rb,
    legend_outside=false,
    one_legend=false,
    colormap=:Nizami,
    linewidth=1,
    noisy_alpha=1
)
    colors = fetch_colors(colormap, length(variables); palette=false)

    figure = Figure(; resolution)
    axis = nothing

    for (variable_index, variable_name) in enumerate(variables)
        axis = Axis(figure[variable_index, 1];
            title=variable_index == 1 ? title : "",
            xlabel=variable_index == length(variables) ? LaTeXString(to_string(time_label)) : "",
            ylabel=LaTeXString(to_string(variables[variable_index])),
            palette=fetch_colors(colormap, 3)
        )

        noisy_label = one_legend ? L"\text{noisy data}" : L"\text{noisy } %$variable_name"
        true_label = one_legend ? L"\text{true data}" : L"\text{true } %$variable_name"
        denoised_label = one_legend ? L"\text{denoised data}" : L"\text{denoised } %$variable_name"

        lines!(axis, time, noisy'[:, variable_index]; label=noisy_label, linewidth)
        lines!(axis, time, truth'[:, variable_index]; label=true_label, linewidth)
        lines!(axis, time, denoised'[:, variable_index]; label=denoised_label, linewidth)

        if legend_outside && !one_legend
            Legend(figure[variable_index, 2], axis)
        elseif !one_legend
            axislegend(; position)
        end
    end

    one_legend && Legend(figure[length(variables)+1, 1], axis; merge=true, orientation=:horizontal)

    figure
end

"""
	function predicted_trajectory_comparison_plot(time, predicted, used, truth, variables;
		resolution=(600, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Predicted trajectory}}"
	)

Plots the comparison of predicted trajectories - all in one axis

**Arguments**:
- `time` - vector of time measurements
- `predicted` - trajectory predicted by discovered SINDy method
- `used` - trajectory used with SINDy method
- `truth` - true trajectory
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
"""
function predicted_trajectory_comparison_plot(time, predicted, used, truth, variables;
    resolution=(600, 450),
    time_label=:t,
    title=L"\text{\textbf{Predicted trajectory}}",
	first_label="predicted",
	second_label="used",
	third_label="true",
    colormap=:Nizami
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        title,
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables)),
        palette=fetch_colors(colormap, length(variables))
    )

    for (variable_index, variable_name) in enumerate(variables)
        lines!(axis, time, predicted'[:, variable_index], label=L"\text{%$first_label } %$variable_name", linewidth=1, color=Cycled(variable_index))
        lines!(axis, time, used'[:, variable_index], label=L"\text{%$second_label } %$variable_name", linestyle=:dash, color=Cycled(variable_index))
        lines!(axis, time, truth'[:, variable_index], label=L"\text{%$third_label } %$variable_name", linestyle=:dot, color=Cycled(variable_index))
    end

    Legend(figure[1, 2], axis)
    figure
end

"""
	function each_predicted_trajectory_comparison_plot(time, predicted, used, truth, variables;
		resolution=(600, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Predicted trajectory}}",
		position=:rb,
		colormap=:Nizami,
		scale_limits=false
	)

Plots the comparison of predicted trajectories - each in a separate axis

**Arguments**:
- `time` - vector of time measurements
- `predicted` - trajectory predicted by discovered SINDy method
- `used` - trajectory used with SINDy method
- `truth` - true trajectory
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
- `scale_limits` - rescales limits of the graph if necessary
"""
function each_predicted_trajectory_comparison_plot(time, predicted, used, truth, variables;
    resolution=(600, 450),
    time_label=:t,
    title=L"\text{\textbf{Predicted trajectory}}",
    position=:rb,
    colormap=:Nizami,
    scale_limits=false
)
    figure = Figure(; resolution)

    for (variable_index, variable_name) in enumerate(variables)
        axis = Axis(figure[variable_index, 1];
            title=variable_index == 1 ? title : "",
            xlabel=variable_index == length(variables) ? LaTeXString(to_string(time_label)) : "",
            ylabel=LaTeXString(to_string(variable_name)),
            palette=fetch_colors(colormap, length(variables))
        )
        lines!(axis, time, used'[:, variable_index], label=L"\text{used } %$variable_name", color=Cycled(2))
        if scale_limits
            autolimits!(axis)
            correct_limits = axis.finallimits[]
        end

        lines!(axis, time, truth'[:, variable_index], label=L"\text{true } %$variable_name", color=Cycled(3))
        lines!(axis, time, predicted'[:, variable_index], label=L"\text{predicted } %$variable_name", color=Cycled(1))

        if scale_limits
            limits!(axis, correct_limits)
        end

        Legend(figure[variable_index, 2], axis)
    end

    figure
end

"""
	function solutionsummary!(axis::Axis, data, time; 
		colormap=:Nizami, 
		trajectory=true, 
		labels=["V", "W"]
	)

Helper function to plot data regarding solution of SINDy model
"""
function solutionsummary!(axis::Axis, data, time;
    colormap=:Nizami,
    labels=["V", "W"]
)
    palette = fetch_colors(colormap, size(data)[1]; palette=false)

    for (index, row) in enumerate(eachrow(data))
        label = index <= length(labels) ? labels[index] : ""
        lines!(axis, time, row; color=palette[index], label=LaTeXString(label))
    end

    return axis
end

"""
	function fitted_model_plot(solution::DataDrivenSolution;
		resolution=(700, 600),
		colormap=:Nizami,
		variables=(:V, :W),
		time_label=:t,
		title=L"\\text{\\textbf{Diagnostic graphs of the discovered model}}",
		position=:rt,
		legend_on_sides=true
	)

Function to plot the DataDrivenSolution (aka solution of the SINDy method). It
plots:

	| trajectories 	| estimated derivatives	|
	|---------------|-----------------------|
	| derivatives 	| errors of derivatives |

**Arguments**:
- `solution::DataDrivenSolution` - solution of the SINDy method

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `legend_on_sides` - determines, whether legend should be above plots or on sides
"""
function fitted_model_plot(solution::DataDrivenSolution;
    resolution=(700, 600),
    colormap=:Nizami,
    variables=(:V, :W),
    time_label=:t,
    title=L"\text{\textbf{Diagnostic graphs of the discovered model}}",
    legend_on_sides=true
)
    lt_plot = legend_on_sides ? (1, 1) : (2, 1)
    lt_legend = legend_on_sides ? (1, 0) : (1, 1)

    legend_move_right = pos -> (pos .+ (legend_on_sides ? (0, 3) : (0, 1)))
    legend_move_down = pos -> (pos .+ (legend_on_sides ? (1, 0) : (3, 0)))
    legend_move_diag = pos -> legend_move_right(legend_move_down(pos))

    plot_move_right = pos -> (pos .+ (0, 1))
    plot_move_down = pos -> (pos .+ (1, 0))
    plot_move_diag = pos -> plot_move_right(plot_move_down(pos))

    system = get_basis(solution)
    parameters = get_parameter_map(system)
    problem = get_problem(solution)

    figure = Figure(; resolution)
    axis1 = Axis(figure[lt_plot...];
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables))
    )
    dataproblemsummary!(axis1, problem; colormap, labels=to_string(variables, format_to="%s(t)", do_join=false))
    Legend(figure[lt_legend...], axis1; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    axis2 = Axis(figure[plot_move_down(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\dot{%s}"))
    )
    dataproblemsummary!(axis2, problem;
        trajectory=false,
        colormap,
        labels=to_string(variables; format_to="\\dot{%s}(t)", do_join=false)
    )
    Legend(figure[legend_move_down(lt_legend)...], axis2; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    fitted_equations = [eq.rhs for eq in equations(system)]
    calc_derivatives = similar(problem.DX)
    for (eq_index, equation) in enumerate(fitted_equations)
        equation_variables = get_variables(equation)
        present_variables = [var for var in variables if var in Symbol.(equation_variables)]

        for (col_index, column) in enumerate(eachcol(problem.X))
            variable_indices = [findfirst(var -> var == variable, Symbol.(equation_variables)) for variable in present_variables]
            states = Dict(equation_variables[variable_indices[index]] => column[index] for (index, variable) in enumerate(present_variables))

            calc_derivatives[eq_index, col_index] = substitute(fitted_equations[eq_index], Dict(parameters..., states...))
        end
    end

    axis3 = Axis(figure[plot_move_right(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}"))
    )
    solutionsummary!(axis3, calc_derivatives, problem.t; colormap, labels=to_string(variables, format_to="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}(t)", do_join=false))
    Legend(figure[legend_move_right(lt_legend)...], axis3; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    axis4 = Axis(figure[plot_move_diag(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="e_{%s}"))
    )
    solutionsummary!(axis4, problem.DX .- calc_derivatives, problem.t; colormap, labels=to_string(variables, format_to="e_{%s}(t)", do_join=false))
    Legend(figure[legend_move_diag(lt_legend)...], axis4; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    Label(figure[0, :], title)

    figure
end

"""
	function noise_comparison_plot(noise, optimizers, parameter_count, R²s, AICs, BICs;
		resolution=(600, 700),
		colormap=:Nizami,
		xlabel=L"\\text{noise}",
		title=L"\\text{\\textbf{Quality of fit based on \\sigma of noise}}",
		kwargs...
	)

Plots comparison (`parameter_count`, `R²s`, `AICs`, `BICs`) of different optimizers based on different amounts of noise

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `title` - title of the entire image
- `xlabel` - label on the x-axis
"""
function noise_comparison_plot(noise, optimizers, parameter_count, R²s, AICs, BICs;
    resolution=(600, 700),
    colormap=:Nizami,
    xlabel=L"\text{noise}",
    title=L"\text{\textbf{Quality of fit based on }} \hat{\sigma} \text{\textbf{ of noise}}",
    wide=false,
	bottom_legend = false,
    kwargs...
)
    optimizer_count = length(optimizers)
    palette = fetch_colors(colormap, optimizer_count; kwargs...)
    colors = fetch_colors(colormap, optimizer_count; palette=false, kwargs...)

    noise = collect(noise)

    figure = Figure(; resolution)

    axis = Axis(figure[1, 1];
        ylabel=L"\text{parameter count}",
        xlabel=wide ? xlabel : "",
        palette
    )
    for (opt_index, optimizer) in enumerate(optimizers)
        lines!(axis, noise, parameter_count[opt_index, :, 1]; label=LaTeXString(to_string(optimizer)))
        band!(axis, noise, parameter_count[opt_index, :, 2], parameter_count[opt_index, :, 3];
            color=(colors[opt_index], 0.2),
            label=LaTeXString(to_string(optimizer))
        )
    end
    !wide && !bottom_legend && Legend(figure[1, 2], axis; merge=true, tellwidth=!wide, tellheight=wide)

    axis = Axis(figure[(wide ? (1, 2) : (2, 1))...];
        ylabel=L"R^2",
        xlabel=wide ? xlabel : "",
        palette
    )
    for (opt_index, optimizer) in enumerate(optimizers)
        lines!(axis, noise, R²s[opt_index, :, 1]; label=LaTeXString(to_string(optimizer)))
        band!(axis, noise, R²s[opt_index, :, 2], R²s[opt_index, :, 3];
            color=(colors[opt_index], 0.2),
            label=LaTeXString(to_string(optimizer))
        )
    end
    !wide && !bottom_legend && Legend(figure[2, 2], axis; merge=true, tellwidth=!wide, tellheight=wide)

    axis = Axis(figure[(wide ? (2, 1) : (3, 1))...];
        ylabel=L"\text{AIC}",
        xlabel,
        palette
    )
    for (opt_index, optimizer) in enumerate(optimizers)
        lines!(axis, noise, AICs[opt_index, :, 1]; label=LaTeXString(to_string(optimizer)))
        band!(axis, noise, AICs[opt_index, :, 2], AICs[opt_index, :, 3];
            color=(colors[opt_index], 0.2),
            label=LaTeXString(to_string(optimizer))
        )
    end
    !wide && !bottom_legend && Legend(figure[3, 2], axis; merge=true, tellwidth=!wide, tellheight=wide)

    (wide || bottom_legend) && Legend(figure[end+1, :], axis; merge=true, orientation=:horizontal)

	if !isnothing(title)
    	Label(figure[0, :], title)
	end

    return figure
end

"""
	function attractor_plot(predicted_first, predicted_second, raw_trajectory, variables;
		resolution=(590, 300),
		title=L"\\text{\\textbf{Attractors of discovered models}}",
		first_label = L"\\text{First model attractor}",
		second_label=L"\\text{Second model attractor}",
		raw_label=L"\\text{Noisy trajectory attractor}",
		colormap=:Nizami,
		legend_below=false,
		raw_color=(:gray, 0.2),
	)

Creates 3D plot that compares attractors of predicted and noisy trajectories

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `title` - title of the entire image

"""
function attractor_plot(predicted_first, predicted_second, raw_trajectory, variables;
    resolution=(590, 300),
    title=L"\text{\textbf{Attractors of discovered models}}",
	first_label = L"\text{First model attractor}",
    second_label=L"\text{Second model attractor}",
    raw_label=L"\text{Noisy trajectory attractor}",
    colormap=:Nizami,
    legend_below=false,
    raw_color=(:gray, 0.2),
	flip = false,
	kwargs...
)
    figure = Figure(; resolution)
    colors = fetch_colors(colormap, 2; palette=false, kwargs...)

    axis = Axis3(figure[1, 1];
        title,
        xlabel=LaTeXString(to_string(variables[1])),
        ylabel=LaTeXString(to_string(variables[2])),
        zlabel=LaTeXString(to_string(variables[3])),
        azimuth=-0.2π,
        protrusions = (5, 30, 15, 0),
		zlabeloffset=29,
        xlabeloffset=27,
        ylabeloffset=27
    )
    lines!(axis, raw_trajectory'; color=raw_color, label=raw_label)
    lines!(axis, predicted_first'; label=first_label, color=colors[flip ? 2 : 1])
    lines!(axis, predicted_second'; label=second_label, color=colors[flip ? 1 : 2])

    Legend(figure[(legend_below ? (2, 1) : (1, 2))...], axis; 
		tellwidth=!legend_below, 
		tellheight=legend_below,
		orientation=:horizontal
	)

    figure
end

function plot_linear_transformation(transformed_trajectory, native_trajectory,
    x_bounds, y_bounds, f;
    resolution=(350, 400),
    colormap=:Nizami,
    title=L"\text{\textbf{Linear transformation attractor}}",
    xlabel=L"V",
    ylabel=L"W",
    position=:rt,
    legend_outside=false,
    stepsize=0.05,
    arrow_size=8,
    maxsteps=40,
    shift_colors=false,
    show_legend=true,
    show_title=true,
    native_label=L"\text{native trajectory}",
    transform_label=L"\text{transformed trajectory}"
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        xlabel,
        ylabel,
        palette=fetch_colors(colormap, 2; rev=true),
        title=show_title ? title : ""
    )
    colors = fetch_colors(colormap, shift_colors ? 3 : 2; palette=false)

    streamplot!(axis, f, x_bounds, y_bounds; stepsize, arrow_size, maxsteps, colormap)

    lines!(axis, Float64.(native_trajectory),
        linewidth=3,
        linestyle=:dash,
        label=native_label,
        color=colors[shift_colors ? 2 : 1]
    )

    lines!(axis, Float64.(transformed_trajectory),
        linewidth=3,
        linestyle=:dash,
        label=transform_label,
        color=colors[end]
    )

    if legend_outside && show_legend
        Legend(figure[2, 1], axis; tellwidth=false, tellheight=true)
    elseif show_legend
        axislegend(; position)
    end

    figure
end

function linear_transformation_comparison(found_trajectory, transformed_trajectory, time;
    resolution=(500, 300),
    colormap=:Nizami,
    title=L"\text{\textbf{Comparison of transformed and discovered trajectory}}",
    variables=(:V, :W),
    time_label=:t,
    position=:rt,
    legend_outside=false,
    legend_onside=false,
    show_legend=true,
    show_title=true,
    nbanks=1
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables)),
        palette=fetch_colors(colormap, 2 * length(variables); rev=true, transform=true)
    )

    for (row_index, trajectory_row) in enumerate(eachrow(found_trajectory))
        transformed_row = transformed_trajectory[row_index, :]
        var = variables[row_index]
        lines!(axis, time, trajectory_row; label=L"\text{discovered } %$var")
        lines!(axis, time, transformed_row; label=L"\text{transformed } %$var")
    end

    if legend_outside && show_legend
        legend_pos = legend_onside ? (1, 2) : (2, 1)
        Legend(figure[legend_pos...], axis; tellwidth=legend_onside, tellheight=!legend_onside, nbanks)
    elseif show_legend
        axislegend(; position, nbanks)
    end

    show_title && Label(figure[0, :], title; tellwidth=false)

    return figure
end

function plot_noise_on_points(data, noisy_data;
    colormap=:Nizami,
    resolution=(280, 280),
    xlabel=L"\text{index}",
    ylabel=L"y",
    markersize=3,
    α=0.5
)
    colors = fetch_colors(colormap, 2; palette=false)

    figure = Figure(; resolution)
    axis = Axis(figure[2, 1]; xlabel, ylabel)

    scatter!(axis, 1:length(noisy_data), noisy_data[:];
        color=colors[2],
        markersize,
        label=L"\text{noisy data}"
    )
    scatter!(axis, data[:];
        color=(colors[1], α),
        markersize,
        label=L"\text{true data}"
    )
    Legend(figure[1, 1], axis; orientation=:horizontal)

    return figure
end


function plot_noise_on_func(x, true_y, noisy_y;
    colormap=:Nizami,
    resolution=(280, 280),
    xlabel=L"x",
    ylabel=L"y",
    func_label=L"x \cdot \sin (x)",
    α=0.3
)
    colors = fetch_colors(colormap, 2; palette=false)

    figure = Figure(; resolution)
    axis = Axis(figure[2, 1]; xlabel, ylabel)

    lines!(axis, x, true_y;
        label=func_label,
        color=colors[1]
    )
    lines!(axis, x, noisy_y;
        color=(colors[2], α),
        label=L"\text{noisy data}"
    )
    Legend(figure[1, 1], axis; orientation=:horizontal)

    return figure
end

function false_phase_portrait(xs, ys, bestfit_y=nothing, bestfit_x=nothing;
    colormap=:Nizami,
    resolution=(200, 280),
    xlabel=L"x",
    ylabel=L"y",
    label=L"\text{trajectory}",
    bestfit_label=LaTeXString("best-fit line"),
    position=:rb,
    legend_outside=false
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        xlabel,
        ylabel,
        palette=fetch_colors(colormap, 2)
    )

    lines!(axis, xs, ys; label, colormap)

    if !isnothing(bestfit_y) && isnothing(bestfit_x)
        lines!(axis, xs, bestfit_y; label=bestfit_label)
    elseif !isnothing(bestfit_y) && !isnothing(bestfit_x)
        lines!(axis, bestfit_x, bestfit_y; label=bestfit_label)
    end

    if legend_outside
        Legend(figure[2, 1], axis; tellwidth=false, tellheight=true)
    else
        axislegend(; position)
    end

    return figure
end