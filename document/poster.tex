\documentclass[final]{beamer}

\input{includes/preambule.tex}
\input{includes/math.tex}

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{DSTLS SINDy Optimizer and regression of Hodgkin-Huxley model onto FitzHugh-Nagumo model} % Poster title

\author{ Štěpán Zapadlo (email:  \href{mailto:stepan.zapadlo@mail.muni.cz}{stepan.zapadlo@mail.muni.cz}), Lenka Přibylová (email:  \href{mailto:pribylova@math.muni.cz}{pribylova@math.muni.cz})} % Author(s)

\institute{Department of Mathematics and Statistics, Faculty of Science, Masaryk University, Brno, Czech Republic} % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\input{includes/lua_macros.tex}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{columns}[t]
	\begin{column}{\twocolwid} % 1. dvojice sloupcu
	\begin{block}{DSTLS SINDy Optimizer}
		\begin{columns}[t,totalwidth=\twocolwid] 
			\setbeamercolor{block title}{fg=dblue,bg=white}
			\vspace*{-\baselineskip}

			\begin{column}{\onecolwid} 
				In the current day and age, we have an abundance of data about many different systems. This data can be mined for useful information and an ODE formulation of the original systems is among the most important. While a myriad of techniques emerged, \textit{Sparse Identification of Nonlinear Dynamics} (SINDy) \cite{BruntonSINDyCore} is one of the most popular, which uses $l$-dimensional regularized linear regression
				\begin{gather*}
					\label{eq:l_dimensional_lsq}
					\min_{\vi \Xi} \frac 1 2 \norm{\dvi X - \vi \Theta(\vi X)\vi \Xi}_2^2 \\
					\constraint \text{sparsity inducing constraint}.
				\end{gather*}
				
				Although it can be solved using different optimization techniques, we propose \textit{Dynamic Sequentially Thresholded Least Squares} (DSTLS); a modification of \textit{Sequentially Thresholded Least Squares} (STLS).

				\bigskip

				\begin{alertblock}{DSTLS optimization problem}
					For a $k$-th variable of our system, we can estimate its derivative using a DSTLS optimization problem:

					\begin{equation*}
						\label{eq:dstls}	
						\begin{gathered}
							\min_{\vi \xi_k} \frac 1 2 \norm{\dvi X_{\cdot, k} - \vi \Theta(\vi X)\vi \xi_k}_2^2\\
							\constraint \forall i \in \set{1, \dots, p}: \quad \absval{{\xi_k}_i} \geq \tau \cdot \max \absval{\vi \xi_k}
						\end{gathered}
					\end{equation*}
				\end{alertblock} 

				%\begin{itemize}[noitemsep,topsep=3pt]
				\phantom{--} \makebox[1em][l]{$\vi X$} measurements of our systems \\
				\phantom{--} \makebox[1em][l]{$\dvi X$} derivatives of our systems \\
				\phantom{--} \makebox[1em][l]{$\vi \Theta$} library of candidate functions\\
				\phantom{--} \makebox[1em][l]{$\vi \xi_k$} $k$-th column of coefficient matrix $\vi \Xi$, corresponding to the equation for the derivative of $k$-th variable \\
				\phantom{--} \makebox[1em][l]{$p$} number of candidate functions\\
				\phantom{--} \makebox[1em][l]{$\tau$} sparsity threshold\\
				%\end{itemize}
				
				\bigskip

				Such modification is motivated by the \textit{FitzHugh-Nagumo} (FHN) model of neuron
				\begin{align*}
					\dot V &= V - \frac {V^3} 3 - W + i_e, \\
					\dot W &= a \cdot (bV - cW + d),
				\end{align*}
				with the following parameters and initial conditions
				\begin{gather*}
					a = 0.08, \, b = 1, \, c = 0.8, \, d = 0.7, \, i_e = 0.8, \\
					V(0) = 3.3, \, W(0) = -2.
				\end{gather*}
				The disparity in magnitudes of parameters between equations causes sensitivity to the value of threshold $\tau$ with the ordinary STLS method. Choosing $\tau$ too big, only a constant zero solution will be discovered for $\dot W$. For $\tau$ small enough unnecessary terms will be identified for $\dot V$. Scaling the threshold by the largest absolute value of estimated parameters aims to address this issue. 

				\begin{block}{Applications and results}
					For the illustration of the proposed method, let us assume the derivatives are unknown and our data are corrupted by a \textit{additive white Gaussian noise} (AWGN) with its variance equal to 5 percent of the data's variance. The derivative is estimated with total variation regularized numerical differentiation, which is computed on raw noisy data, and smooth the data using total variation. At last, polynomials up to 4th order were used as a candidate library. DSTLS optimizer correctly chooses the appropriate candidate functions, unlike STLS. 

					\bigskip

					\codeSnippet{juliacode}{fhn_generator.jl}
					\codeSnippet{juliacode}{fhn_learner.jl}

					\begin{alertblock}{Comparison of discovered models}
						\small
						\begin{tabular}{l|c|}
							\cline{2-2}
							\rule{0pt}{2.5cm}\rule[-1.9cm]{0pt}{1.9cm}
							$\begin{aligned}
								\tryinput{generated/tex_system_dstls.tex}
							\end{aligned}$ %
							\rule{.35cm}{0pt}
							& \rule{.05cm}{0pt} \rotatebox[origin=c]{90}{DSTLS} \rule{.05cm}{0pt} \\
							\hline
							\rule{0pt}{3cm}\rule[-2.4cm]{0pt}{2.4cm}
							$\begin{aligned}
								\tryinput{generated/tex_system_stls.tex}
							\end{aligned}$ %
							\rule{.35cm}{0pt}
							& \rule{.05cm}{0pt} \rotatebox[origin=c]{90}{STLS} \rule{.05cm}{0pt} \\
							\cline{2-2}
						\end{tabular}
					\end{alertblock}
					
				\end{block}
			\end{column} 

			\begin{column}{\sepwids}\end{column}

			\begin{column}{\onecolwid}
				Furthermore, a comparison of STLS, DSTLS, and SR3 \cite{zheng2018unified} optimizers was performed with respect to the variance of AWGN.

				\codeSnippet{juliacode}{fhn_noises_generator.jl}
				\codeSnippet{juliacode}{fhn_noises_learner.jl}

				\begin{figure}
					\raggedleft
					\SaveAndPlot{figures}{fhn_noises}{figure}{width=0.975\\textwidth}
					\caption{\, The effect of the magnitude of applied noise and used optimizer on the behavior of SINDy learning the FHN model. The DSTLS optimizer best retains sought qualities of low parameter count, high $R^2$ and low AIC with a rising variance of noise in the FitzHugh-Nagumo model.}
					\label{fig:noise_optimizer_effect_fhn}
				\end{figure}

				\vspace{-\baselineskip}
				\begin{block}{Effects on Lorenz system}
					Although DSTLS was designed to be used on models with differences in magnitudes of parameters between equations, it still reliably works in other cases. As an example, we show its behavior on the well-known Lorenz model:
					\begin{align*}
						\dot{x} &= \sigma (y - x), \\
						\dot{y} &= x(\rho - z) - y, \\
						\dot{z} &= xy - \beta z
					\end{align*}
					with the standard choice of chaos inducing parameters $\sigma = 10,\rho = 28, \beta = \frac 8 3$. The derivatives are unknown and trajectory data is affected by AWGN with variance equal to 1 percent of the data variance. Polynomials up to 3rd order were used as a candidate library. Due to the chaotic nature of the said system, prediction of the exact same trajectory with the discovered model cannot be expected, but the goal is to identify the attractor reasonably well. Only trajectories lying on the attractor were used for training.

					\codeSnippet{juliacode}{lorenz_generator.jl}
					\codeSnippet{juliacode}{lorenz_learner.jl}
					
					\begin{figure}
						\centering
						\SaveAndPlot{figures}{lorenz_attractor}{figure}{width=0.85\\textwidth}
						\label{fig:lorenz_attractor}
					\end{figure}					
				\end{block}

				\vspace{-1.5\baselineskip}
				\begin{block}{Acknowledgements}
					This work is supported by GAMU Interdisciplinary project \# MUNI/G/1213/2022 and Mathematical and Statistical Modeling project \# MUNI/A/1132/2022.
				\end{block}
			\end{column}
		\end{columns}
	\end{block}
	\end{column}
	% konec 1.dvojsloupce

	%\begin{column}{\sepwids} \end{column}

	\begin{column}{\onecolwid} 
		\begin{block}{Transformations of HH onto FHN model with Optim.jl} 
			\setbeamercolor{block title}{fg=dblue,bg=white}
			The highly nonlinear 4D \textit{Hodgkin-Huxley} (HH) system is a physiological neuron model \cite{HodgkinHuxley}, unlike its conceptual simplification in the form of the 2D polynomial FHN model. As such, we strive to fit these two models to obtain physiological parameters for the FHN model as well. 

			\bigskip

			\begin{alertblock}{Transformation optimization problem}
				The trajectory $\vi H$ of the HH model is presumed to be the truth and we strive to find both the transformation $\vi \Lambda \in \R^{4 \times 2}$ and the parameters $\vi p$ for FHN model with a trajectory $\vi u(T; \vi p)$ as a solution to the problem
				\begin{equation}
					\label{eq:hh_to_fhn_optim}
					\min_{\vi \Lambda, \vi p} \loss (\vi u(T; \vi p), \vi H \vi \Lambda),
				\end{equation}
				such that for $\vi x, \vi y \in \R^{k \times 2}$ the loss function reads
				\begin{gather*}
					\begin{aligned}
						\loss(\vi x, \vi y) = \frac 1 k \norm{\vi x - \vi y}_2^2 &+ \alpha \osc (\vi x, \vi y) \\
						&+ \beta \osc (\vi x_{1 \dots \floor{\frac k 2}, \cdot}, \vi x_{\floor{\frac k 2} + 1 \dots k, \cdot}),
					\end{aligned} \\
					\osc(\vi a, \vi b) = \absval{1 - \frac {\variance (\vi a)}{\variance (\vi b)}}
				\end{gather*}
			\end{alertblock}

			The optimization problem \eqref{eq:hh_to_fhn_optim} has a non-convex loss function and due to the inclusion of the simulated trajectory $\vi u(T; \vi p)$ of the FHN model, computation of the gradient would be expensive. Therefore we employed the Nelder-Mead method from the Optim.jl package, which does not require the gradient.

			\codeSnippet{juliacode}{hh_generator.jl}
			\codeSnippet{juliacode}{hh_nm_learner.jl}
			
			\begin{figure}
				\centering
				\SaveAndPlot{figures}{nm_phase_plot}{figure}{width=0.82\\textwidth}
				\label{fig:nm_phase_plot}
			\end{figure}	


			\vspace{-\baselineskip}
			\begin{block}{Enhancing fit with Metaheuristics.jl}
				While the Nelder-Mead method works reasonably well, in our case it happens to be very sensitive to the initial condition, i.e. initial transformation matrix $\vi \Lambda$ and parameters $\vi p$. To overcome this issue, we utilized the \textit{Whale Optimization Algorithm} (WOA) from the Metaheuristics.jl package, which can be thought of as using an ensemble of initial conditions.

				\codeSnippet{juliacode}{hh_heuristic_learner.jl}
				
				\begin{figure}
					\centering
					\SaveAndPlot{figures}{heuristic_phase_plot}{figure}{width=0.82\\textwidth}
					\label{fig:heuristic_phase_plot}
				\end{figure}	
			\end{block}

			\vspace{-2.5cm}
			\begin{block}{References}
				\renewcommand*{\bibfont}{\small}
				\printbibliography
			\end{block}
		\end{block}
	\end{column}
\end{columns}
\end{document}
