import DataDrivenSparse: init_cache, step!
import LinearAlgebra: I

"""
	Base.@kwdef struct DSTLS <: Optimizer
**Dynamic STLS (DSTLS)**

Fields:
- `thresholds` - coefficients under threshold times the highest coefficient are zeroed out (in each variable separately)
"""
struct DSTLS{T<:Union{Number,AbstractVector},R<:Number} <:
       DataDrivenSparse.AbstractSparseRegressionAlgorithm
    """Sparsity threshold"""
    thresholds::T
    """Ridge regression parameter"""
    rho::R


    function DSTLS(threshold::T=1e-1, rho::R=zero(eltype(T))) where {T,R<:Number}
        @assert all(threshold .> zero(eltype(threshold))) "Threshold must be positive definite"
        @assert rho >= zero(R) "Ridge regression parameter must be positive definite!"
        return new{T,R}(threshold, rho)
    end
end

Base.summary(::DSTLS) = "DSTLS"
Base.show(io::IO, opt::DSTLS) = print(io, "DSTLS($(opt.thresholds), $(opt.rho))")

"""
	struct DSTLSCache{usenormal,C<:AbstractArray,A<:BitArray,AT,BT,ATT,BTT} <:
		DataDrivenSparse.AbstractSparseRegressionCache
		X::C
		X_prev::C
		active_set::A
		proximal::SoftThreshold
		A::AT
		B::BT
		# Original Data
		Ã::ATT
		B̃::BTT
	end

Cache to be used along with DSTLS and speed it up

Implementation heavily inspired by existing STLSQ code
"""
struct DSTLSCache{usenormal,C<:AbstractArray,A<:BitArray,AT,BT,ATT,BTT} <:
       DataDrivenSparse.AbstractSparseRegressionCache
    X::C
    X_prev::C
    active_set::A
    proximal::SoftThreshold
    A::AT
    B::BT
    # Original Data
    Ã::ATT
    B̃::BTT
end

function init_cache(alg::DSTLS, A::AbstractMatrix, b::AbstractVector)
    init_cache(alg, A, permutedims(b))
end

function init_cache(alg::DSTLS, A::AbstractMatrix, B::AbstractMatrix)
    n_x, m_x = size(A)
    @assert size(B, 1) == 1 "Caches only hold single targets!"
    @unpack rho = alg
    λ = minimum(DataDrivenSparse.get_thresholds(alg))

    proximal = DataDrivenSparse.get_proximal(alg)

    if n_x <= m_x && !iszero(rho)
        X = A * A' + rho * I
        Y = B * A'
        usenormal = true
    else
        usenormal = false
        X = A
        Y = B
    end

    coefficients = Y / X

    prev_coefficients = zero(coefficients)

    active_set = BitArray(undef, size(coefficients))

    DataDrivenSparse.active_set!(active_set, proximal, coefficients, λ)

    return DSTLSCache{usenormal,typeof(coefficients),typeof(active_set),typeof(X),
        typeof(Y),typeof(A),typeof(B)}(coefficients, prev_coefficients,
        active_set, DataDrivenSparse.get_proximal(alg),
        X, Y, A, B)
end

"""
	function step!(cache::DSTLSCache, λ::T) where {T}

Step of DSTLS method using cache
"""
function step!(cache::DSTLSCache, λ::T) where {T}
    @unpack X, X_prev, active_set, proximal = cache

    X_prev .= X

    step!(cache)

    ξₕ = maximum(abs.(X))
    proximal(X, active_set, λ * ξₕ)
    return
end

function step!(cache::DSTLSCache{true})
    @unpack X, A, B, active_set = cache
    p = vec(active_set)
    X[1:1, p] .= /(B[1:1, p], A[p, p])
    return
end

function step!(cache::DSTLSCache{false})
    @unpack X, A, B, active_set = cache
    p = vec(active_set)
    X[1:1, p] .= /(B, A[p, :])
    return
end